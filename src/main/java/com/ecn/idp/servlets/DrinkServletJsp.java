package com.ecn.idp.servlets;

import com.ecn.idp.entities.Drink;
import com.ecn.idp.service.DrinkService;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/drink")
public class DrinkServletJsp extends HttpServlet {


    /*@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String title = "Simple Servlet ";

        // J'indique que le type de reponse que je construit est de l'HTML
        resp.setContentType("text/html");
    }*/

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String nameParameter = req.getParameter("name"); // Note : tous les paramètres sont **toujours** récupérés sous forme de String !!
        String providerParameter = req.getParameter("provider");
        String originParameter = req.getParameter("origin");
        String alcoolPercentParameter = req.getParameter("alcoolPercent");
        String commentParameter = req.getParameter("comment");
        String stockParameter = req.getParameter("stock");

        int stock = 0;
        try {
            stock = Integer.valueOf(stockParameter); // Je tente de basculer de la String à l'int
        } catch (NumberFormatException e) {
            // Si ma String n'est pas un entier : une exception est levée, il faut la traiter
            System.out.println("On a eu un stock qui n'est pas numérique : " + stockParameter);
        }

        float alcoolPercent = 0;
        try {
            alcoolPercent = Float.valueOf(alcoolPercentParameter); // Je tente de basculer de la String à l'int
        } catch (NumberFormatException e) {
            // Si ma String n'est pas un entier : une exception est levée, il faut la traiter
            System.out.println("On a eu un pourcentage qui n'est pas un double : " + alcoolPercentParameter);
        }

        DrinkService drinkService = new DrinkService();
        drinkService.createDrink(nameParameter, providerParameter, originParameter, alcoolPercent, commentParameter, stock);

        // Envoyer les infos vers la JSP
        List<Drink> allDrinks = drinkService.findAllDrinks();
        req.setAttribute("drinks", allDrinks);


        // Déléguer la suite du traitement à la JSP
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/WEB-INF/jsp/drink.jsp");
        rd.forward(req, resp);

    }

}

