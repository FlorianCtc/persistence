package com.ecn.idp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Entity
public class Drink {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;
    protected String name;
    protected String provider;
    protected String origin;
    protected float alcoolPercent;
    @Column(columnDefinition = "TEXT")
    protected String comment;
    protected int stock;

    public Drink() { }

    public Drink(String name, String provider, String origin, float alcoolPercent, String comment, int stock) {
        this.name = name;
        this.provider = provider;
        this.origin = origin;
        this.alcoolPercent = alcoolPercent;
        this.comment = comment;
        this.stock = stock;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public float getAlcoolPercent() {
        return alcoolPercent;
    }

    public void setAlcoolPercent(float alcoolPercent) {
        this.alcoolPercent = alcoolPercent;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Drink{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", provider='" + provider + '\'' +
            ", origin='" + origin + '\'' +
            ", alcoolPercent=" + alcoolPercent +
            ", comment='" + comment + '\'' +
            ", stock='" + stock + '\'' +
            '}';
    }
}
