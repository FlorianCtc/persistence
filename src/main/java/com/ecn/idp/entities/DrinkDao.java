package com.ecn.idp.entities;

import com.ecn.JpaUtil;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */

public class DrinkDao {

    // Note : afin de laisser la gestion de la transaction au niveau des services,
    // Je donne en paramètres l'entityManager en cours d'utilisation (pour utiliser
    // le même "tuyau de communication" avec la base que la transaction

    public Integer create(Drink drink, EntityManager em) {

        // Lorsque j'enregistre en BDD, l'instance est modifiée avec l'id généré par la BDD
        em.persist(drink);// INSERT INTO ...

        return drink.getId();
    }

    public List<Drink> getAll(EntityManager em) {

        TypedQuery<Drink> query = em.createQuery("FROM Drink", Drink.class);
        List<Drink> result = query.getResultList();

        return result;
    }

    public Drink get(Integer id, EntityManager em) {

        // Lorsque j'enregistre en BDD, l'instance est modifiée avec l'id généré par la BDD
        Drink drink = em.find(Drink.class, id);// INSERT INTO ...

        return drink;
    }

    public void update(Drink drink, EntityManager em) {
        em.merge(drink);
    }

    public Drink getByName(String name, EntityManager em) {

        // Je crée ma requete pour aller chercher les boissons dont le nom (mis en minuscules) contient le terme envoyé en paramètre (en minuscule aussi)
        TypedQuery<Drink> query = em.createQuery("FROM Drink WHERE lower(name) = :param", Drink.class);
        // Dans ma requete, j'ai utilisé un paramètre "param", je lui donne la valeur de la variable envoyé dans la méthode
        query.setParameter("param", name.toLowerCase());
        List<Drink> list = query.getResultList();
        Drink result = list.isEmpty() ? null : list.get(0); // Si ma liste est vide : je retourne null, sinon je retourne le premier
        return result;
    }

    public List<Drink> getAllByName(String name, EntityManager em) {

        // Je crée ma requete pour aller chercher les boissons dont le nom (mis en minuscules) contient le terme envoyé en paramètre (en minuscule aussi)
        TypedQuery<Drink> query = em.createQuery("FROM Drink WHERE lower(name) LIKE :param", Drink.class);
        // Dans ma requete, j'ai utilisé un paramètre "param", je lui donne la valeur de la variable envoyé dans la méthode, que j'encadre avec les "%" pour le LIKE
        query.setParameter("param", "%" + name.toLowerCase() + "%");
        List<Drink> list = query.getResultList();
        return list;
    }
}
