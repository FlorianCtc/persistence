package com.ecn.idp.service;

import com.ecn.JpaUtil;
import com.ecn.idp.entities.Drink;
import com.ecn.idp.entities.DrinkDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DrinkService {

    private DrinkDao drinkDao = new DrinkDao();

    // Correspond à la fonctionnalité 1
    public void createDrink(String name, String provider, String origin, float alcoolPercent, String comment, int stock) {

        Drink newDrink = new Drink(name, provider, origin, alcoolPercent, comment, stock);

        // Je décide de gérer ma transaction ici
        EntityManager entityManager = JpaUtil.getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        Integer id = drinkDao.create(newDrink, entityManager);

        transaction.commit();

        entityManager.close();
    }

    // Fonctionalité 2
    public List<Drink> findAllDrinks() {
        EntityManager entityManager = JpaUtil.getEntityManager();
        List<Drink> drinks = drinkDao.getAll(entityManager);
        entityManager.close();
        return drinks;
    }

    // Fonctionalité pas demandée :)
    public List<Drink> findAllDrinksByName(String name) {
        EntityManager entityManager = JpaUtil.getEntityManager();
        List<Drink> drinks = drinkDao.getAllByName(name, entityManager);
        entityManager.close();
        return drinks;
    }

    // Fonctionalité 3
    public void drinkMyDrink(Integer drinkId, int nb) {
        EntityManager entityManager = JpaUtil.getEntityManager();

        // Je commence par rechercher la boisson demandée en bdd
        Drink drink = drinkDao.get(drinkId, entityManager);
        if (drink == null) {
            throw new EntityNotFoundException();
        }
        if (drink.getStock() == 0) {
            //en vrai, j'ai la flemme de faire l'exception
            //throw new NotMoreStockException();
        }

        // Je diminue le stock de 1
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        int newStock = drink.getStock() - nb;
        drink.setStock(newStock);
        drinkDao.update(drink, entityManager);
        transaction.commit();
        entityManager.close();

    }

    // Correspond à la fonctionnalité 4
    public void createDrinkV2(String name, String provider, String origin, float alcoolPercent, String comment, int stock) {

        EntityManager entityManager = JpaUtil.getEntityManager();

        // Je commence par chercher si j'ai deja une boisson avec le nom

        Drink existing = drinkDao.getByName(name, entityManager);
        if (existing == null) {
            createDrink(name, provider, origin, alcoolPercent, comment, stock);

        } else {
            // Je diminue le stock du nombre fourni
            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            int newStock = existing.getStock() + stock;
            existing.setStock(newStock);
            drinkDao.update(existing, entityManager);
            transaction.commit();
            entityManager.close();
        }
    }
}
