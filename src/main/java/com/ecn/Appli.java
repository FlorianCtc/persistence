package com.ecn;

import com.ecn.idp.entities.Drink;
import com.ecn.idp.service.DrinkService;

import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Appli {

    public static void main(String[] args) {

        // ICi, je vais jouer avec ce que me permet mon service
        DrinkService service = new DrinkService();

        // Mon service me permet de créer des boissons à partir des différentes données que je lui envoie (comme si j'avais saisi un formulaire)
        service.createDrink("Pepsi Max", "Pepsi", "USA", 0f, null, 50);
        service.createDrink("Rochefort 10", "Abbaye trappiste Notre Dame de St REMY", "Belgique", 11.5f, "Acajou foncé, limpide (sur levure), mousse beige fugace, bonne saturation. Intensité liquoreuse remarquable, caramélisée. Saveur moelleuse, puissante, bouche ample, épicée, légèrement acre et piquante. Notes fruitées de prune mûre et cacao. Amertume assez  faible. Corps fin, noble et racé. Un must !", 30);
        service.createDrink("Rochefort 8", "Abbaye trappiste Notre Dame de St REMY", "Belgique", 9.2f, "Couleur acajou, limpide, mousse fine et stable, bien saturée. Nez doux et épicé, aux tons de cacao. Arôme délicat, fin et équilibré. Saveur d'abord piquante, épicée, puis douceur chocolatée et rondeur de  malt   caramel  très nettes. Corps long, doux et harmonieux. Très bonne bière trappiste.", 30);

        // Je peux connaitre mon stock général
        List<Drink> allDrinks = service.findAllDrinks();
        System.out.println(allDrinks);

        // Je consomme : premier paramètre : id de la boisson (ici : 1), second paramètre : le nombre que je consomme (2)
        service.drinkMyDrink(1, 2);

        System.out.println("==== Conso ====");
        // Je récupère l'état de mon stock pour l'afficher
        allDrinks = service.findAllDrinks();
        for (Drink drink : allDrinks) { // Je vais afficher chaque boisson de manière individuelle pour plus de lisibilité
            System.out.println(drink);
        }


        // Ajout d'une boisson existante
        service.createDrinkV2("Pepsi Max", "Pepsi", "USA", 0f, null, 25);
        System.out.println("==== Ajout de boisson existante ====");
        // Je récupère l'état de mon stock pour l'afficher
        allDrinks = service.findAllDrinks();
        for (Drink drink : allDrinks) { // Je vais afficher chaque boisson de manière individuelle pour plus de lisibilité
            System.out.println(drink);
        }

        System.out.println("==== Recherche des boissons contenant 'rochefort' dans le nom ====");
        // Je récupère l'état de mon stock pour l'afficher
        allDrinks = service.findAllDrinksByName("rochefort");
        for (Drink drink : allDrinks) { // Je vais afficher chaque boisson de manière individuelle pour plus de lisibilité
            System.out.println(drink);
        }

        JpaUtil.closeFactory();// Fermeture de la factory, termine proprement l'appli ;)
    }

}
