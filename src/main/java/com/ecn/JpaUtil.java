package com.ecn;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class JpaUtil {

    private static EntityManagerFactory entityManagerFactory;

    public static void resetEntityManagerFactory() {
        if (entityManagerFactory !=null && entityManagerFactory.isOpen()) {
            entityManagerFactory.close();
        }
        entityManagerFactory =
            Persistence.createEntityManagerFactory("tp-persistence");
    }

    public static EntityManager getEntityManager() {
        if (entityManagerFactory == null) {
            resetEntityManagerFactory();
        }
        return entityManagerFactory.createEntityManager();
    }

    // Permet de fermer la factory et libérer les connections
    public static void closeFactory() {
        entityManagerFactory.close();
    }
}
