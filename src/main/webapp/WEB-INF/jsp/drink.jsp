<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Premiere Jsp</title>
  </head>
  <body>

<div>

    <ul>
        <c:forEach var="drink" items="${drinks}">
            <li> ${drink.name} </li>
            <li> ${drink.provider} </li>
            <li> Degré : ${drink.alcoolPercent}</li>
            <li> Commentaire : ${drink.comment}</li>
            <li> Stock : ${drink.stock}</li>
        </c:forEach>
    </ul>

</div>

  </body>
</html>