# « On va boire un verre ou prendre un pot au bar... Hum ? Un p’tit godet là »

Parler ça me donne soif, alors je me dis que ce serait pas mal d'avoir un stock de boissons :beers: !

Le but de ce TP sera donc de gérer ce stock de boisson !

Avant tout, définissons ce que sera une boisson :
* Un nom (obligatoire, ie: non nullable)
* Un fournisseur
* Un pourcentage d'alcool
* Origine
* Éventuellement, un commentaire

Et bien sur, nous aurons un entier nous permettant de connaitre notre stock.



## Mise en place

Je vous laisse démarrer ce projet comme bon vous semble :
* vous pouvez créer un nouveau projet (maven)
* vous pouvez faire une branche sur ce projet (et gérer la création de l'arborescence maven)


Dans l'idéal, j'aimerais une classe `DrinkService` qui mettre à disposition les
différentes méthodes correspondant aux fonctionnalités. (ie: couche métier)
Cette classe fera ensuite appel au(x) classe(s) de DAO de l'application : parce
que peut être prochainement nous pourrons y greffer une interface web qui sait ?
(Si besoin, on en parle, mais c'est pas une priorité)


### Fonctionnalité n°1 : Création du stock

Pour consommer, il faut avoir du stock...
Pour commencer, nous aimerions pouvoir ajouter des boissons à notre stock.
il faudra donc pouvoir renseigner les différentes informations concernant la
boisson à ajouter en base de donnée, ainsi que sa quantité en stock.


### Fonctionnalité n°2 : Consultation du stock

Ajouter des boissons, c'est bien, savoir ce qu'on a en stock c'est mieux !
Il nous faudrait être capable de fournir la liste des boissons avec leur
quantité en stock.


### Fonctionnalité n°3 : Consommation

On a notre carte/liste des boissons, on peut choisir et consommer ?

Il nous faut pouvoir dire qu'on consomme une boisson, en fournissant son id.
L'effet produit, c'est que le stock diminue.


### Fonctionnalité n°4 : Mise à jour du stock

Bon, le stock va se mettre à diminuer... Nous pouvons toujours ajouter
des boissons, mais il faudrait éviter de se retrouver avec plusieurs lignes
dans la liste concernant la même boisson avec des stocks différents.

Nou aimerions donc que l'application reconnaisse lors de l'ajout d'une
boisson si celle-ci est déjà présente en stock (ie : en base de données).
Pour déterminer l'existence d'une boisson, nous nous fierons simplement à
son nom. Si une boisson avec le même nom existe déjà, nous mettrons simplement
son stock à jour (en le créditant du nouveau stock renseigné dans le
formulaire).

ie : on modifie le service de création, pour qu'il recherche d'abord en BDD si
une boisson du même nom existe, et si oui, c'est une mise à jour du stock plutot
qu'une création.


### Fonctionnalité n°5 : Le fournisseur

Actuellement, le fournisseur est un champs texte simple, auquel on peut associer
l'origine. Nous aimerions apporter plus de consolidation à ce couple
fournisseur/origine en faisant de lui un objet distinct Fournisseur, avec les
caractéristiques suivantes :
* Nom
* Pays
* Ville
* Conditonnement

Il s'agit donc de modifier notre modèle pour intégrer une relation 1-N entre
Boisson et Fournisseur.

Il nous faut donc pouvoir créer un Fournisseur, et pouvoir récupérer ses infos
(par son id et/ou par son nom).


### Fonctionnalité n°6 : Diversification des boissons

En fait, nous avons plusieurs types de boissons, avec des caractéristiques propres.
On retouve :
* Les bières, avec pour caractéristiques supplémentaires :
  * Type (ex: Trappiste, spéciale...)
  * Fermentation (ex: Haute, triple, ...)
  * Volume de bouteille en cl (ex: 33, 75, ... )
* Les sodas :
  * Avec ou sans sucre
  * Parfum
  * Pétillant ou non
* Les cafés
  * Type (expresso, court, allongé, ...)
  
À vous de mettre en place de l'héritage depuis Boisson et de choisir la
meilleure stratégie pour enregistrer ça en base de données !



Santé !